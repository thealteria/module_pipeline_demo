#!/bin/bash
echo "Fetching and Testing Flutter..."
flutter doctor
flutter clean
flutter pub get
flutter test
echo "Fetching and Testing done"