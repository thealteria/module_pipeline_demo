#!/bin/bash
if [ "${BITBUCKET_PR_DESTINATION_BRANCH}" != "main" ]
then 
   echo 'not a target branch we want to check'
   exit
# replace main with your desired branch to run the tests
else 
   sh ./script.sh
fi